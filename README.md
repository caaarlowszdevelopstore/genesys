Genesys [![Build Status](https://travis-ci.org/Caaarlowsz/Genesys.png)](https://travis-ci.org/Caaarlowsz/Genesys)
===========

High performance Minecraft server implementation


How To
-----------

Init a Genesys-API and Genesys-Server module : `git submodule update --init`




Compilation
-----------

We use maven to handle our dependencies.

* Install [Maven 3](http://maven.apache.org/download.html)
* Clone this repo and: `mvn clean install`
